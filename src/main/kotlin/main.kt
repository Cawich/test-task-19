fun main(args: Array<String>) {
    println(findUnsortedSubarrayViaCopyArray(  arrayOf(1, 2, 1, 6, 5, 4, 7, 8, 9, 7, 11, 7)))
}

fun findUnsortedSubarrayViaCopyArray(input: Array<Int>) : SubArrayLimits {
    val result = SubArrayLimits(0, 0)
    var leftFound = false
    var forwardIndex: Int = 0
    val sortedArray = input.copyOf().sortedArray()
//    println(input.forEach { print(" $it") })
//    println(sortedArray.forEach { print(" $it") })
    sortedArray.forEachIndexed { index, value ->
        if (input[index] != value) {
            if (!leftFound) {
                result.left = index
                leftFound = true
            }
            forwardIndex = index
        }
        if (leftFound && input[index] == value) {
            result.right = index - 1
        }
        if (forwardIndex > result.right) {
            result.right = forwardIndex
        }
    }
    return result
}

data class SubArrayLimits(
    var left: Int,
    var right: Int
)